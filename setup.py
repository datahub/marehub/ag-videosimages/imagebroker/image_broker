import setuptools

setuptools.setup(
    name="image_broker",
    version="0.1.1",
    author="Stefan Lingner",
    author_email="slingner@geomar.de",
    description="image broker",
    packages=setuptools.find_packages(),
    include_package_data=True,
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    python_requires='>=3.6',
    install_requires=[
        'Werkzeug<3.0.0', # temporary fix for newest flask version - required because flask doesn't specify the dependency correctly (the requirements say Werkzeug>=2.2.0, but when Werkzeug releases a new version, the latest version of flask may not be compatible)
        'Flask_RESTful',
        'Flask',
        'Flask-IIIF',
        'elements-sdk>=3.4.0, <3.5.0',
        'requests',
        'python-dotenv'
    ]
)
