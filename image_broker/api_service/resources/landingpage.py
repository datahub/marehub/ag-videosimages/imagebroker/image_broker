from flask_restful import Resource
from flask import render_template, make_response, request
from urllib.parse import urlparse


class LandingpageResource(Resource):
    def get(self):
        # for ha proxy
        urls = request.url_root.split(", ")
        urlparse(urls[0]).netloc
        if len(urls) > 1:
            url = 'https://' + \
                urlparse(urls[0]).hostname+'/'+urls[1].split("/", 1)[1]
        else:
            url = urls[0]

        headers = {'Content-Type': 'text/html'}
        return make_response(render_template('index.html', url=url), 200, headers)
