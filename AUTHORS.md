# Credits

## Core Team 
- Stefan Lingner \<slingner@geomar.de\>
- Karl Heger \<kheger@geomar.de\>
- Claas Faber \<cfaber@geomar.de\>

## Contributors

None yet. Why not be the first?
