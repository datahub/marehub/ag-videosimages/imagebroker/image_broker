FROM python:3.12-slim
LABEL maintainer="Stefan Lingner <datamanagement@geomar.de>"

RUN useradd --create-home --shell /bin/bash myuser
USER myuser
WORKDIR /home/myuser
ENV PATH="/home/myuser/.local/bin:${PATH}"

COPY --chown=myuser:myuser . .
RUN pip install --upgrade pip
RUN pip install -e .

EXPOSE 5000

# -u prints the log in the docker output
CMD [ "python","-u", "./run.py" ]