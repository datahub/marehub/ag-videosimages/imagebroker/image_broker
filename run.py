from image_broker import image_broker_app

if __name__ == "__main__":
    image_broker_app.run(host='0.0.0.0')
