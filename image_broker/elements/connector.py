import elements_sdk
from elements_sdk.api import media_library_api
from elements_sdk.exceptions import ApiException
from flask import abort


def get_api_client(elements_url: str, api_token: str):
    """Creates api_client instance with provided credentials
    Params:
        elements_url str: Base url of your elements-medialib instance
        api_token str: Access token for your elements-medialib instance
    """
    configuration = elements_sdk.Configuration()
    # Configure API key authorization: Bearer
    configuration.api_key["Authorization"] = api_token
    configuration.api_key_prefix["Authorization"] = "Bearer"

    configuration.host = elements_url

    # Enter a context with an instance of the API client
    with elements_sdk.ApiClient(configuration) as api_client:
        return api_client


def get_bundle_by_custom_field_match(api_client: "elements_sdk.ApiClient", custom_field: str, value: str, verbose: bool = False):
    """ Searches for file matching the custom field value. Returns bundle object if file found, None otherwise 
    Params:
        api_client elements_sdk.ApiClient: elements_sdk.ApiClient instance configured w/ url and credentials
        custom_field str: name of custom field
        value str: value of custom field to match
        verbose bool=False: if true prints message
    """

    #api_instance = elements_sdk.MediaLibraryApi(api_client)
    api_instance = media_library_api.MediaLibraryApi(api_client)

    # fields used in web server search, for some reason not all supported in elements_sdk 3.4.0
    advanced_search = "[{\"type\":\"custom-field\",\"fieldName\":\"" + \
        custom_field + "\",\"lookup\":\"any\",\"query\":\"" + value + "\"}]"
    #effective_custom_fields = True
    exclude_deleted = False           # yes?
    exclude_unrecognized = False
    include_modified_by = True
    limit = 50
    offset = 0
    ordering = "name"
    resolve_asset_permission = True

    ret = api_instance.get_all_media_file_bundles(ordering=ordering,
                                                  limit=limit,
                                                  offset=offset,
                                                  exclude_deleted=exclude_deleted,
                                                  exclude_unrecognized=exclude_unrecognized,
                                                  advanced_search=advanced_search)

    bunlde = None
    exactMatches = 0
    for match in ret:
        # the search does not necessarily only return exact matches
        if match.asset.custom_fields[custom_field] == value:

            if is_offline(api_client, match):
                if verbose:
                    print("Skipping offline file!")
                continue

            exactMatches += 1
            if exactMatches >= 2:
                if verbose:
                    print("Caution! Multiple matches found, only first is returned")
                break
            bunlde = match

    if bunlde is None and verbose:
        print("No match found")

    return bunlde


def is_offline(api_client: "elements_sdk.ApiClient", mediafile):
    """Checks mediafile or bundle metadata the mediafile/bundle is offline. Optionally considers offline status of parents.

        Params:
            api_client elements_sdk.ApiClient: elements_sdk.ApiClient instance configured w/ url and credentials
            mediafile: mediafile or bundle object as returned by elements api

        returns: True if medieafile/bundle is offline, or if parents are offline, False if mediafile/bundle is online
    """
    # check if bundle or mediafile obj was passed
    if 'mainfile' in mediafile.attribute_map:
        mediafile = mediafile.mainfile

    return not mediafile.present


def get_download_link_to_custom_field_match(api_client: "elements_sdk.ApiClient", custom_field: str, value: str, verbose: bool = False):
    """ Searches for file matching the custom field value. Returns download link to file if file found, empty string otherwise 
    Params:
        api_client elements_sdk.ApiClient: elements_sdk.ApiClient instance configured w/ url and credentials
        custom_field str: name of custom field
        value str: value of custom field to match
        verbose bool=False: if true prints message
    """
    bundle = None
    try:
        bundle = get_bundle_by_custom_field_match(
            api_client, custom_field, value, verbose)
    except KeyError:
        abort(404)
    except ApiException as e:
        print('Probably wrong login credentials')
        abort(500)

    if bundle is None:
        return ""

    downloadLink = api_client.configuration.host + \
        "/api/2/media/files/" + str(bundle.mainfile.id) + "/download"
    return downloadLink


def get_easyShare_download_link_to_custom_field_match(api_client: "elements_sdk.ApiClient", custom_field: str, value: str, verbose: bool = False):
    """ Searches for file matching the custom field value. Returns (and creates if not there yet) easy share link to file if file found, empty string otherwise 
    Params:
        api_client elements_sdk.ApiClient: elements_sdk.ApiClient instance configured w/ url and credentials
        custom_field str: name of custom field
        value str: value of custom field to match
        verbose bool=False: if true prints message
    """

    bundle = get_bundle_by_custom_field_match(
        api_client, custom_field, value, verbose)
    if bundle is None:
        return ""

    api_instance = elements_sdk.MediaLibraryApi(api_client)
    ret = api_instance.get_easy_sharing_token_for_bundle(bundle.id)
    return ret.full_url
