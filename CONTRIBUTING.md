# Contributing

Contributions are welcome, and they are greatly appreciated! Every
little bit helps, and credit will always be given.

You can contribute in many ways:

## Types of Contributions

### Report Bugs

Report bugs at https://gitlab.hzdr.de/datahub/marehub/imagebroker/image_broker/-/issues.

If you are reporting a bug, please include:

-   Your operating system name and version.
-   Any details about your local setup that might be helpful in
    troubleshooting.
-   Detailed steps to reproduce the bug.

### Fix Bugs

Look through the GitLab issues for bugs. Anything tagged with \"bug\"
and \"help wanted\" is open to whoever wants to implement it.

### Implement Features

Look through the GitLab issues for features. Anything tagged with
\"enhancement\" and \"help wanted\" is open to whoever wants to
implement it.

### Write Documentation

The image broker could always use more documentation,
whether as part of the official docs, in
docstrings, or even on the web in blog posts, articles, and such.

### Submit Feedback

The best way to send feedback is to file an issue at
https://gitlab.hzdr.de/datahub/marehub/imagebroker/image_broker/-/issues.

If you are proposing a feature:

-   Explain in detail how it would work.
-   Keep the scope as narrow as possible, to make it easier to
    implement.
-   Remember that
    contributions are welcome :)


## Merge Request Guidelines

Before you submit a merge request, check that it meets these guidelines:

1.  The merge request should include tests.
2.  If the merge request adds functionality, the docs should be updated.
    Put your new functionality into a function with a docstring, and add
    the feature to the list in README.md.
3.  The merge request should work for Python 3.8. Check https://gitlab.hzdr.de/datahub/marehub/imagebroker/image_broker/-/merge_requests and make sure that the tests pass for all supported
    Python versions.

## Tips

To run a subset of tests:

``` {.shell}
```

> \$ pytest [tests.test]()image_broker



## Deploying

A reminder for the maintainers on how to deploy. Make sure all your
changes are committed (including an entry in HISTORY.md). Then run:

``` {.shell}
$ bump2version patch # possible: major / minor / patch
$ git push
$ git push --tags
```

Gitlab-CI will then deploy to Gitlab registry if tests pass.
