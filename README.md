# MAMS Image Broker

The GEOMAR Helmhotz Centre for Ocean Research Kiel is operating a Media Asset Management System (MAMS) which manages large amounts image- and video data. Similar systems are operated by the Helmholtz Centre HEREON and by the Alfred Wegener Institute (AWI). Although the MAMS system provides access to data and metadata using an API, it is not possible to directly request image data without prior knowledge of the internal MAMS data structure.  
The image broker is a web service which brokers between a client (e.g. web-browser) and the MAMS. It allows users to request images by metadata values (e.g. image uuid). The broker uses the [IIIF](https://iiif.io/) standard which allows users to request the images in different formats, scaled copies or only specific parts of the image.

## Use case: access to iFDO images via handle server

At GEOMAR, a handle server provides persistent URLs for images. These persistent identifiers are for example stored in image Fair Digital Objects ([iFDOs](https://www.marine-imaging.com/fair/ifdos/iFDO-overview/)). The image broker brokers between the handle server and the Elemens MAMS that physically stores the image data. It does so by using the `<image-uuid>` from the iFDO metadata that is known in Elements and embedded into the image file headers.

How to grant access permissions to broker for certain data in the Elements MAMS is described [here](https://idrz-wiki.geomar.de/books/dfd-workflows/page/image-broker).

Discussions that led to the implementation of the image broker for GEOMAR are available [here](https://git.geomar.de/dm/datahub/marehub-imagesvideos/-/blob/master/handles-for-images.md) and [here](https://notes.desy.de/r8t2zc6FSLyr2TEEzdezzw).

## Key features of the image broker of GEOMAR

- Uses the [IIIF](https://iiif.io/) standard
- built with [Flask-IIIF](https://flask-iiif.readthedocs.io/)
- Landingpage: https://osis.geomar.de/images/broker/
- URL: https://osis.geomar.de/images/broker/v1
- Corresponding handle URL: https://hdl.handle.net/20.500.12085/ee277578-a911-484d-a515-9c781d79aa91
- Usage: calling https://hdl.handle.net/20.500.12085/ee277578-a911-484d-a515-9c781d79aa91@ `<image-uuid>` returns the binary image with the given UUID (if known in Elements)

### Big picture

![](https://s3.desy.de/hackmd/uploads/upload_cd7dae24e26f1cc4d12b0bfafd118202.png)
Note: the URLs are not correct. Those were still drafts. Correct URLs are given in the feature list above.

### Docker

#### Docker development

docker build -t image_broker .

docker run -e TOKEN='token' -e MEDIALIB_URL='https://medialib.geomar.de' -p5000:5000 image_broker

GET localhost:5000/images/v1/image-uuid/5621f4af-f8bb-477c-ae57-9d9217819af7/full/500,/0/default.jpg

#### Docker production
Flask provides a development server, that is not designed to be particularly efficient, stable, or secure.
For production, a WSGI server should be used.
Additionally, it is recommended to run the WSGI server as a reverse proxy, with Nginx or Apache in front of it.
Furthermore, enabling caching at the web server can lead to significant performance improvements.

Example setup for production:
nginx -> docker -> gunicorn -> image_broker

In this example, gunicorn is used as WSGI server inside a docker container.
```
docker build -f Dockerfile_production -t broker .

docker run -e TOKEN='token' -e MEDIALIB_URL='https://medialib.geomar.de' -e WEB_CONCURRENCY=4 -p8000:8000 broker
Note: WEB_CONCURRENCY: number of gunicorn workers

curl -i http://localhost:8000/v1/image-uuid/5621f4af-f8bb-477c-ae57-9d9217819af7/full/500,/0/default.jpg

Note: "Cache-Control: no-cache" is returned by default in the reponse header. To enable caching on the webserver, ignore the cache-control header.
      I.e., Nginx:
           proxy_ignore_headers  "Cache-Control";
           proxy_hide_header  "Cache-Control";
           proxy_cache_valid  200 24h;

// Production
docker run --restart always -d -e TOKEN='token' -e MEDIALIB_URL='https://medialib.geomar.de' -e WEB_CONCURRENCY=16 -p8000:8000 broker
```
