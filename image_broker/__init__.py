from image_broker.reverse_proxied import ReverseProxied
from image_broker.api_service.routes import initialize_routes

import os
from flask_restful import Api
from flask_iiif import IIIF
__version__ = "0.1.1"

from flask import Flask
image_broker_app = Flask(__name__)
ext = IIIF(app=image_broker_app)
image_broker_app.config['VERSION'] = __version__
image_broker_app.url_map.strict_slashes = False  # Disable strict slash handling globally

# fix for reverse proxy pass
if os.environ.get("PROXY_PASS_PREFIX"):
    image_broker_app.wsgi_app = ReverseProxied(
        image_broker_app.wsgi_app, script_name=os.environ.get("PROXY_PASS_PREFIX"), scheme='https')

api = Api(image_broker_app)

initialize_routes(api)
