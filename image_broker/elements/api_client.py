
from cmath import atan
from image_broker.elements.connector import get_api_client
import os
import signal
import atexit

token = os.environ.get('TOKEN')
medialib_url = os.environ.get('MEDIALIB_URL')
api_client = get_api_client(medialib_url, api_token=token)


def close_api(*args):
    print("closing")
    api_client.close()
    raise KeyboardInterrupt()

try:
    # this works in docker deployments
    signal.signal(signal.SIGTERM, close_api)
    signal.signal(signal.SIGINT, close_api)
    
except ValueError:
    # this works with `flask run``
    atexit.register(close_api)
    

    
