class ReverseProxied(object):
    """
      Fixes url_for calls if behind a reverse proxy.
      Solution as proposed in
      https://blog.macuyiko.com/post/2016/fixing-flask-url_for-when-behind-mod_proxy.html

        use like this:
        app.wsgi_app = ReverseProxied(app.wsgi_app, script_name=os.environ.get("/prefix_from_proxypass"))

      If using factory pattern for flask app generation,
      place call above in the block with application context.
    """

    def __init__(self, app, script_name=None, scheme=None, server=None):
        self.app = app
        self.script_name = script_name
        self.scheme = scheme
        self.server = server

    def __call__(self, environ, start_response):
        script_name = environ.get('HTTP_X_SCRIPT_NAME', '') or self.script_name
        if script_name:
            environ['SCRIPT_NAME'] = script_name
            path_info = environ['PATH_INFO']
            if path_info.startswith(script_name):
                environ['PATH_INFO'] = path_info[len(script_name):]
        scheme = environ.get('HTTP_X_SCHEME', '') or self.scheme
        if scheme:
            environ['wsgi.url_scheme'] = scheme
        server = environ.get('HTTP_X_FORWARDED_SERVER', '') or self.server
        if server:
            environ['HTTP_HOST'] = server
        return self.app(environ, start_response)
