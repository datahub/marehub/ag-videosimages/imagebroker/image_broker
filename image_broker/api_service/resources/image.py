from flask_restful import Resource
from flask import send_file, abort, Response
from flask_iiif.api import IIIFImageAPIWrapper
from io import BytesIO
import requests
import os
from image_broker.elements.connector import get_download_link_to_custom_field_match
from image_broker.elements.api_client import api_client


class IiifImageResource(Resource):
    def get(self, search_field, search_string, region, size, rotation, quality_format):
        if region.lower() != 'original':
            quality_format = quality_format.split('.')
            quality = quality_format[0]
            format = quality_format[1]
        token = os.environ.get('TOKEN')

        dl_link = get_download_link_to_custom_field_match(
            api_client, search_field, search_string)
        if not dl_link:
            abort(404)

        headers = {"Authorization": f"Bearer {token}"}
        r = requests.get(dl_link, stream=True, headers=headers)

        # Check if the image was retrieved successfully
        if r.status_code == 200:
            # Set decode_content value to True, otherwise the downloaded image file's size will be zero.
            r.raw.decode_content = True
            iiif_image = IIIFImageAPIWrapper.from_string(BytesIO(r.raw.data))

            if format == 'default':
                format = iiif_image.image.format.lower()

            iiif_image.apply_api(
                region=region,
                size=size,
                rotation=rotation,
                quality=quality
            )

            return send_file(iiif_image.serve(image_format=format), mimetype=f'image/{format}')
        else:
            abort(404)

    @staticmethod
    def _return_original_image(original_response: requests.Response):
        """Re-uses the original Elements response as a flask response"""
        # Create a Flask response with the same content, headers, and status code
        flask_response = Response(
            response=original_response.content,  # Forward content
            status=original_response.status_code,  # Forward status code
            content_type=original_response.headers.get('Content-Type')  # Forward content type
        )

        # Add any other headers from the external response (optional)
        for key, value in original_response.headers.items():
            flask_response.headers[key] = value

        return flask_response


class OriginalImageResource(Resource):
    def get(self, search_field, search_string):
        token = os.environ.get('TOKEN')

        dl_link = get_download_link_to_custom_field_match(
            api_client, search_field, search_string)
        if not dl_link:
            abort(404)

        headers = {"Authorization": f"Bearer {token}"}
        original_response = requests.get(dl_link, stream=True, headers=headers)
        # Create a Flask response with the same content, headers, and status code
        flask_response = Response(
            response=original_response.content,  # Forward content
            status=original_response.status_code,  # Forward status code
            content_type=original_response.headers.get('Content-Type')  # Forward content type
        )

        # Add any other headers from the external response (optional)
        for key, value in original_response.headers.items():
            flask_response.headers[key] = value

        return flask_response