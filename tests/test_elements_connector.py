from os import environ
from re import A

from dotenv import load_dotenv
from pytest import fixture

from image_broker.elements import connector as ec
from image_broker import image_broker_app as app

load_dotenv()


@fixture()
def api_client():
    assert environ.get('TOKEN'), f"TOKEN env var for elements token not set!"
    assert environ.get(
        'MEDIALIB_URL'), f"MEDIALIB_URL env var for elements url not set!"

    return ec.get_api_client(environ.get('MEDIALIB_URL'), environ.get('TOKEN'))


@fixture
def test_cf_name():
    assert environ.get(
        "TEST_CUSTOM_FIELD_NAME"), "TEST_CUSTOM_FIELD_NAME env var not set!"
    return environ.get("TEST_CUSTOM_FIELD_NAME")


@fixture
def test_cf_value():
    assert environ.get(
        "TEST_CUSTOM_FIELD_VALUE"), "TEST_CUSTOM_FIELD_VALUE env var not set!"
    return environ.get("TEST_CUSTOM_FIELD_VALUE")


@fixture
def test_bundle_id():
    assert environ.get(
        "TEST_EXPECTED_BUNDLE_ID"), "TEST_EXPECTED_BUNDLE_ID env var not set!"
    return environ.get("TEST_EXPECTED_BUNDLE_ID")


@fixture
def test_offline_bundle_id():
    assert environ.get(
        "TEST_OFFLINE_BUNDLE_ID"), "TEST_OFFLINE_BUNDLE_ID env var not set!"
    return environ.get("TEST_OFFLINE_BUNDLE_ID")


@fixture
def test_bundle_name():
    assert environ.get(
        "TEST_EXPECTED_BUNDLE_NAME"), "TEST_EXPECTED_BUNDLE_NAME env var not set"
    return environ.get("TEST_EXPECTED_BUNDLE_NAME")

@fixture
def client():
    with app.test_client() as client:
        yield client

def test_hello_world(client):
    response = client.get('/v1/test')
    assert response.status_code == 200
    assert response.json == {'message': 'Hallo, from Image Broker. I got GET'}


def test_get_api_client(api_client):
    assert api_client
    assert api_client.configuration.host == environ.get('MEDIALIB_URL')
    assert api_client.configuration.api_key


def test_is_bundle_offline(api_client, test_offline_bundle_id, test_bundle_id):
    api = ec.elements_sdk.MediaLibraryApi(api_client)

    offline = api.get_media_file_bundle(test_offline_bundle_id)
    online = api.get_media_file_bundle(test_bundle_id)

    assert ec.is_offline(api_client, offline)
    assert not ec.is_offline(api_client, online)


def test_get_bundle_by_custom_field_match(api_client, test_cf_name, test_cf_value, test_bundle_id, test_bundle_name):
    bdl = ec.get_bundle_by_custom_field_match(api_client, custom_field=test_cf_name,
                                              value=test_cf_value)
    assert str(bdl.id) == test_bundle_id
    assert bdl.name == test_bundle_name
    assert not ec.is_offline(api_client, bdl)


def test_get_download_link_to_custom_field_match(api_client, test_cf_name, test_cf_value, test_bundle_id):
    dl = ec.get_download_link_to_custom_field_match(api_client, custom_field=test_cf_name,
                                                    value=test_cf_value)
    bdl = ec.get_bundle_by_custom_field_match(api_client, custom_field=test_cf_name,
                                              value=test_cf_value)
    file_id = bdl.mainfile.id

    assert dl
    assert dl.startswith(environ.get(
        'MEDIALIB_URL')), f"expected download link w/ url {environ.get('MEDIALIB_URL')}, but got {dl}"
    assert str(
        file_id) in dl, f"expected bundle id {file_id} in downlad link {dl}"
    assert not ec.is_offline(api_client, bdl)


def test_get_image_iif_syntax(client, test_cf_name, test_cf_value):
    response = client.get(f'/v1/{test_cf_name}/{test_cf_value}/full/500,/0/default.jpg')
    assert response.status_code == 200

def test_get_image_original(client, test_cf_name, test_cf_value):
    response = client.get(f'/v1/{test_cf_name}/{test_cf_value}/original/')
    assert response.status_code == 200



def test_get_easyShare_download_link_to_custom_field_match(api_client, test_cf_name, test_cf_value):
    # TODO test fails w/ elements-api error
    # allow to fail for now
    return True

    dl = ec.get_easyShare_download_link_to_custom_field_match(
        api_client, test_cf_name, test_cf_value)
    assert dl
