from flask_restful import Resource
from flask import jsonify


class TestResource(Resource):
    def get(self):
        return jsonify({'message': 'Hallo, from Image Broker. I got GET'})

    def put(self):
        return jsonify({'message': 'Hallo, from Image Broker. I got PUT'})

    def post(self):
        return jsonify({'message': 'Hallo, from Image Broker. I got POST'})

    def delete(self):
        return jsonify({'message': 'Hallo, from Image Broker. I got DELETE'})
