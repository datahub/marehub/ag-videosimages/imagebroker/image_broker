from image_broker.api_service.resources.image import IiifImageResource, OriginalImageResource
from image_broker.api_service.resources.test import TestResource
from image_broker.api_service.resources.landingpage import LandingpageResource


def initialize_routes(api):
    api.add_resource(IiifImageResource,
                     '/v1/<string:search_field>/<string:search_string>/<string:region>/<string:size>/<string:rotation>/<string:quality_format>')

    api.add_resource(OriginalImageResource,
                     '/v1/<string:search_field>/<string:search_string>/original')


    api.add_resource(TestResource,
                     '/v1/test')

    api.add_resource(LandingpageResource,
                     '/')


""" @image_broker_app.after_request
def after_request(response):
    response.direct_passthrough = False  # important for swagger
    log_level = logger.level
    if log_level <= 30:
        log_request(response, log_level)
    return response """
